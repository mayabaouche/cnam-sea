# Projet Minishell

## Contexte

Il s'agissait d'implémenter un interpréteur de commande similaire à **bash** en C. 

## Mayshell

### Outils utilisés 

Afin de développer le projet, j'ai utilisé un éditeur de texte, **Geany**. 
Afin de compiler les sources, j'ai créé un **Makefile** pour automatiser la compilation. 
Afin de tester et de lancer mon projet, j'ai utilisé l'interpréteur de commande déjà existant.
Pour générer la documentation, j'ai utilisé **Doxygen**.

### Démarches 

L'approche mise en place pour ce projet est la suivante: 
- L’évaluation de l’expression soumise à l’interpréteur à l'aide de la fonction strtok.
- L’exécution ordonnancée du sous-ensemble de commandes à l'aide d'une liste chainée.
- La soumission du résultat d’exécution à l'utilisateur.

### Fonctionnalités implémentées 

Le binaire est capable de : 

- exécuter une commande simple (ie : ls –l ; ps ; who)
- exécuter un sous-ensemble de plusieurs commandes de sorte à prendre en
compte :
    * Les opérateurs de contrôle : && et || et ; 
    * Les redirections de flux simples : |, >, <, >>
    * L’exécution en arrière-plan : &   


- exécuter les commandes internes (fonctionnalités built-in) suivantes :
    * **cd** - Permettant de se déplacer au sein d’une arborescence de fichier.
    * **pwd** – Affichant la valeur de la variable contenant le chemin du répertoire courant.
    * **exit** – Permettant de quitter l’interpréteur.
    * **echo** – Permettant d’afficher du texte sur la sortie standard.  
    

- fournir la persistance des commandes saisie dans un fichier historique (**.shell_history**). 

- de se lancer en mode batch (ie : ./shell –c « ls –al | grep toto »).

- une page de manuel présente le Mayshell et son utilisation.
 
### Documentation

La documentation est disponible [ici](https://mayabaouche.gitlab.io/cnam-sea/).
### Difficultés rencontrées

* Le shell est capable de prendre en compte seulement 3 ensembles de commandes. 
En effet, avec la structure actuelle, il n'est pas possible de gérer un cas tel que : 

> cd .. || ls -l | sort || echo no 


* Le code n'est pas couvert par des tests. Je n'ai pas réussi à mettre en place les tests avec l'outils gcov.

