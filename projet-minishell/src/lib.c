/**
 * \file lib.h
 * \brief Functions definitions.
 * \author Maya.B
 * \version 0.1
 * \date 06 décembre 2019
 *
 * Define functions' used in project. 
 *
 */
#include "lib.h"

void free_parsed_input(node_t* head)
{
	node_t* tmp;
	// loops over the linked list
	while (head != NULL)
	{
		// free command argv cell by cell
		for (unsigned int i = 0; i < head->argc; i++)
		{
			if (head->argv[i] != NULL)
			{
				free(head->argv[i]); 			
			}
			head->argv[i] = NULL;
				
		}	
		//	free current node's argv
		if (head->argv != NULL)
			free(head->argv);
		head->argv = NULL;
		
		tmp = head;
		head = head->next;
		// free last node
		if (tmp != NULL) 		
			free(tmp); 
		tmp = NULL; 	
	}
}

void read_semicolon(node_t* current) 
{
	int status; 
	// if current node is not yet executed
    if(current != NULL && !current->is_executed) 
    {
		// runs it 
        pid_t res = run_current_command(current);
        // waits for it to end
        waitpid(res, &status, 0);
    }
}

void read_and(node_t* current)
{
	pid_t res = -1; 
	int status; 
	// if current node is not yet executed
	if (current != NULL && !current->is_executed )
	{
		// runs it 
		res = run_current_command(current); 
		// waits for it to end
        waitpid(res, &status, 0);
        // success is defined by child exit status
        current->success = WIFEXITED(status) && WEXITSTATUS(status) == 0;
	}
	// if current node is executed and successful and if next is not
	// yet executed
	if (current->is_executed && current->success 
		&& current->next != NULL && !current->next->is_executed )
	{
		// runs next command
		res = run_current_command(current->next); 
		// if control operator encountered is not a redirection op 
		// nor a background command
		if (current->next->control_op != AMPERSAND && current->next->control_op != PIPE)
		{
			// waits for child to end
			waitpid(res, &status, 0);
			// success if child exit status
			current->success = WIFEXITED(status) && WEXITSTATUS(status) == 0;		
		}
	}
	// if current failed
	else if (current->is_executed && !current->success)
	{
		// next will fail too
		current->next->is_executed = true; 
		current->next->success = false;  
	}
}

void read_or(node_t* current)
{	
	pid_t res = -1; 
	int status; 
	// executes if not executed yet
	if (!current->is_executed)
	{ 
		res = run_current_command(current); 
        waitpid(res, &status, 0);
        current->success = WIFEXITED(status) && WEXITSTATUS(status) == 0; 
	} 
	// if current failed and next is not yet executed
	if (current->is_executed && !current->success
		&& current->next != NULL && !current->next->is_executed)
	{
		// runs next command
		res = run_current_command(current->next); 
		// if next command is not before a redirect nor a background execution		
		if (current->next->control_op != AMPERSAND && current->next->control_op != PIPE)
		{
			waitpid(res, &status, 0);
			current->success = WIFEXITED(status) && WEXITSTATUS(status) == 0;		
		}
	}
	// if current is successful, no need to execute next command
	else if (current->is_executed && current->success)
	{
		current->next->is_executed = true; 
		current->next->success = true; 
	}	
}

void read_pipe(node_t* current)
{
	
    int fd[2];
	// pipe creation
    if(pipe(fd) == -1) 
    {
        print_error("pipe() failed.", errno); 
        exit(EXIT_FAILURE);
    }
    // assigns pipe entries
    // from current to next direction
    current->fd_out = fd[1];
    current->next->fd_in = fd[0];
    
	int status; 
	// if current command is not executed yet, runs it
    if(current != NULL && !current->is_executed) 
    {
        pid_t res = run_current_command(current);	
        waitpid(res, &status, 0);
    }
	// closes file descriptor
	if (current->fd_out != -1)
		close(current->fd_out);
}

void read_ampersand(node_t* current)
{ 
    if(current != NULL && !current->is_executed) 
    {
		// semicolon behaviour without the wait part
        run_current_command(current);
    }
}

char* trim_white_space(char *str)
{
  char *end;

  // Trim leading space
  while(isspace((unsigned char)*str)) 
	str++;

  if(*str == 0)  // All spaces?
    return str;

  // Trim trailing space
  end = str + strlen(str) - 1;
  while(end > str && isspace((unsigned char)*end)) 
	end--;

  // Write new null terminator character
  end[1] = '\0';

  return str;
}

pid_t fork_and_execute_command(node_t* current)
{
	// created child
    pid_t child = fork();

    if(child == -1) 
    {
        print_error("fork() failed.", errno);
    } 
    // in child
    else if(child == 0) 
    {
        char* args[current->argc+1]; 		
		for (unsigned int i = 0 ; i < current->argc; i++)
		{ 
			args[i] =  trim_white_space(current->argv[i]);  
		}
		args[current->argc] = NULL;
		
		// assigns command descriptors
		// to standard input and output
		if (current->fd_in != -1)
			dup2(current->fd_in, STDIN_FILENO); 
		if (current->fd_out != -1)
			dup2(current->fd_out, STDOUT_FILENO); 
		
		// executes command	
		execvp(args[0], args);
		print_error("Could not execute command.", errno);     
		exit(EXIT_FAILURE);
    }
    // close to make order
    if (current->fd_in != -1)
		close(current->fd_in);
	if (current->fd_out != -1)
		close(current->fd_out); 	
    return child;
}


pid_t exit_shell()
{
	printf("Goodbye!\n"); 
	exit(EXIT_SUCCESS); 
	return -1;
}

pid_t echo_message(node_t* current)
{
	pid_t child = fork(); 
	if (child == 0)
	{
		// assigns command file descriptors
		if (current->fd_in != -1)
			dup2(current->fd_in, STDIN_FILENO); 
		if (current->fd_out != -1)
			dup2(current->fd_out, STDOUT_FILENO);
				
		// printfs echo params		
		for (unsigned int i = 1; i < current->argc; i++)
		{
			current->argv[i] = trim_white_space(current->argv[i]);
			// minimalistic implementation of echo...
			printf("%s ", current->argv[i]);
		}
		printf("\n");
		// close descriptors
		if (current->fd_in != -1)
			close(current->fd_in);
		if (current->fd_out != -1)
			close(current->fd_out);
		exit(EXIT_SUCCESS);
	}
	// close descriptors
	if (current->fd_in != -1)
		close(current->fd_in);
		
	if (current->fd_out != -1)
		close(current->fd_out);
	return child; 
}

pid_t change_directory(node_t* current)
{		
	// not enought args for cd
	if (current->argc < 2)
	{
		current->success = false; 
	}
	else 
	{
		// retrieve path
		current->argv[1] = trim_white_space(current->argv[1]);
		// checks
		if (strlen(current->argv[1]) > PATH_MAX)
		{
			current->success = false; 
			print_error("Path name too long.", errno);
		}
		// change directory
		else if(chdir(current->argv[1]) < 0)
		{
			current->success = false; 
			print_error("Could not change directory.", errno); 	
			
		}
		else 
			current->success = true; 
	}
		
	pid_t child = fork();  
	if (child == 0)
	{
		exit(current->success ? EXIT_SUCCESS : EXIT_FAILURE);
	}
	return child; 
}

pid_t print_working_directory(node_t* current)
{
	pid_t child = fork();  
	if (child == 0)
	{
		if (current->fd_in != -1)
			dup2(current->fd_in, STDIN_FILENO); 
		if (current->fd_out != -1)
			dup2(current->fd_out, STDOUT_FILENO);
			
		char current_directory[PATH_MAX];
		// gets current working directory
		if (getcwd(current_directory, sizeof(current_directory)) != NULL) 
		{
			printf("%s\n", current_directory);
			current->success = true; 
			if (current->fd_in != -1)
				close(current->fd_in);
			if (current->fd_out != -1)
				close(current->fd_out);
			exit(EXIT_SUCCESS);
		} 
		else 
		{
			current->success = false; 			
			if (current->fd_in != -1)
				close(current->fd_in);
			if (current->fd_out != -1)
				close(current->fd_out);
			print_error("Get current working directory failed.", errno);
			exit(EXIT_FAILURE);
		}
	}
	if (current->fd_in != -1)
		close(current->fd_in);
	if (current->fd_out != -1)
		close(current->fd_out);
	
	return child; 
}

void print_error(const char* message, unsigned int code)
{
	printf("Error : %s \nCode : %d - %s\n", message, code, strerror(code));
}

node_t* init_empty_node()
{
    node_t* current_node = malloc(sizeof(node_t)); 
	current_node->argc = 0; 
	// max args count to receive
	current_node->max_argc = MAX_ARGS_COUNT; 
	current_node->argv = (char**)malloc(MAX_ARGS_COUNT*sizeof(char*));
	for (int i = 0; i < MAX_ARGS_COUNT; i++)
	{
		current_node->argv[i] = NULL;
	}
	current_node->is_executed = false; 
	current_node->success = false;
	current_node->fd_in = -1; 
	current_node->fd_out = -1;
	// default behaviour
	current_node->control_op = SEMICOLON;
	current_node->next = NULL;
    return current_node; 
}

void push_node(node_t* previous, node_t* new)
{
	previous->next = new; 
}

operator_t read_operator(char* token)
{	
	token = trim_white_space(token); 	
	if (strcmp(token, "&&") == 0)
		return AND; 
	else if (strcmp(token, "||") == 0)
		return OR; 
	else if(strcmp(token, ";") == 0)
		return SEMICOLON; 
	else if (strcmp(token, "&") == 0)
		return AMPERSAND;
	else if (strcmp(token, "|") == 0)
		return PIPE;
	else if (strcmp(token, ">") == 0)
		return R_REDIRECT; 
	else if (strcmp(token, ">>") == 0)
		return DR_REDIRECT;
	else if (strcmp(token, "<") == 0)
		return L_REDIRECT; 
	else if (strcmp(token, "<<") == 0)
		return DL_REDIRECT;
	else 
		return COMMAND; 
}

pid_t run_current_command(node_t* current)
{	
	current->is_executed = true;
	if (current->argc > 0)
	{
		switch(is_built_in(current->argv[0]))
		{
			case CD: 	return change_directory(current); 
			case PWD: 	return print_working_directory(current); 
			case ECHO:	return echo_message(current); 
			case EXIT: 	return exit_shell(); 
			case NONE:	return fork_and_execute_command(current);
						
		}
	}
	return -1;  
}

built_in_t is_built_in(char* arg)
{
	arg = trim_white_space(arg); 
	
	if (strcmp(arg, "cd") == 0)
		return CD; 
	else if (strcmp(arg, "pwd") == 0)
		return PWD;
	else if (strcmp(arg, "echo") == 0)
		return ECHO; 
	else if (strcmp(arg, "exit") == 0)
		return EXIT; 
	else 	
		return NONE;
}

void execute_nodes(node_t* root)
{
	node_t* head = root; 
	// runs list of command
	while (head != NULL)
	{
		// checks next control operator in command line
		switch(head->control_op)
		{
			case SEMICOLON: 				
				read_semicolon(head);
				break; 
			case AND: 
				read_and(head); 
				break;
			case OR: 
				read_or(head); 
				break; 
			case PIPE: 
				read_pipe(head);
				break;
			case AMPERSAND:
				read_ampersand(head);
				break;
			default:
				read_semicolon(head); 
				break; 	
		}
		head = head->next; 
	}
}

node_t* parse_input(char* input)
{ 
	// used to limit number of commands 
	int command_count = 0;
	// head of parsed input linked list
	node_t* root = init_empty_node(); 
	// used for loop   
	node_t* current =  NULL;
	current = root;
	// read first token 
	char* line = strtok(input, " "); 
	
	do {	 
		operator_t current_operator = read_operator(line);
		switch (current_operator)
		{
			case COMMAND:
			{
				// allocate more memory into array if insufiscient 
				if (current->argc == current->max_argc)
				{
					current->max_argc += MAX_ARGS_COUNT;
					current->argv = (char**)realloc(current->argv, current->max_argc*sizeof(char*));
					for (unsigned int i = current->argc; i < current->max_argc; i++)
					{
						current->argv[i] = NULL;
					}
				}
				
				size_t len = strlen(line)+1;
				
				if (len > 1 && strlen(trim_white_space(line)) > 0)
				{					
					// add current token
					current->argv[current->argc] = (char*)malloc(len*sizeof(char));		
					strcpy(current->argv[current->argc], line); 
					// increase args count
					current->argc++; 
				}
				
			}			
			break; 
			
			case AND: 
			case OR:
			case SEMICOLON:
			case AMPERSAND:
			case PIPE:
			{
				current->control_op =  current_operator;
				// add node to linked list
				node_t* new_node = init_empty_node();
				push_node(current, new_node);
				command_count++;  
				current = new_node; 
			}
			break;
			
			case R_REDIRECT:
			{				
				// read next token 
				line = strtok(NULL, " "); 
				line = trim_white_space(line);
				int fd = open(line, O_WRONLY | O_CREAT | O_TRUNC, 0644);
				if (fd < 0)
				{
					print_error("Could not open file.", errno);
					return NULL; 
				}
				else 
				{
					current->fd_out = fd; 
				}
				
			}
			break; 
			case DR_REDIRECT:
			{				
				// read next token 
				line = strtok(NULL, " "); 
				int fd = open(line, O_WRONLY | O_CREAT | O_APPEND, 0644);
				if (fd < 0)
				{
					print_error("Could not open file.", errno);
					return NULL; 
				}
				else 
				{
					current->fd_out = fd; 
				}
				
			}
			break; 
			case L_REDIRECT:
			{				
				// read next token 
				line = strtok(NULL, " "); 
				int fd = open(line, O_RDONLY, 0644);
				if (fd < 0)
				{
					print_error("Could not open file.", errno);
					return NULL; 
				}
				else 
				{
					current->fd_in = fd; 
				}
				
			}
			break;
			case DL_REDIRECT:
				
			break;
			default:
			
			{
				current->control_op =  current_operator;
				// add node to linked list
				node_t* new_node = init_empty_node();
				push_node(current, new_node);
				command_count++;  
				current = new_node; 
			}
			break;
					
		}
		
		// read next token 
		line = strtok(NULL, " "); 		
						
	} while (line != NULL && command_count < 3);
	
	if (line != NULL) 
	{
		printf("Shell supports 3 sub-commands. Input was trimmed.\n");
	}
	return root; 
}

void display_message()
{
	printf("|***********************|\n");
	printf("|  Welcome to MayShell  |\n");
	printf("|***********************|\n");
}

void check_usage_mode(int args_count, char** args)
{
	// if there is enough args for batch mode
	if ( 2 < args_count && strcmp(args[1], "-c") == 0)
	{
		printf("Your are using batch mode.\n"); 
		// write in history
		int fd = open(".shell_history", O_CREAT | O_WRONLY | O_TRUNC, 0664);
		if (fd > 0)
		{
			write(fd, args[2], strlen(args[2]));
			fsync(fd);
		}		
		close(fd); 
		// do parsing and execution			
		node_t* input = malloc(sizeof(node_t));
		input = parse_input(args[2]);
		execute_nodes(input);		
		free_parsed_input(input);

	}
	else 
	{
		use_interactive_mode(); 
	}
}
void use_interactive_mode()
{
	char *line = (char*)malloc(sizeof(char*));
	char cwd[PATH_MAX];
	int fd = open(".shell_history", O_CREAT | O_WRONLY | O_TRUNC, 0664);
    do
	{
		printf("%s@MS:%s$ ", getenv("USERNAME"), getcwd(cwd, sizeof(cwd)));	
		fflush(stdout);   
		fgets(line, CONSOLE_BUFFER_SIZE, stdin); 
		 
		write(fd, line, strlen(line));
		fsync(fd);
		// do parsing and execution
		node_t* input = malloc(sizeof(node_t));
		input = parse_input(line);
		execute_nodes(input);
		free_parsed_input(input);
	} 
	while (1);
	close(fd);
	free(line);
}
