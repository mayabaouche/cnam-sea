/**
 * \file lib.h
 * \brief Signature definitions.
 * \author Maya.B
 * \version 0.1
 * \date 06 décembre 2019
 *
 * Define functions' signatures used in project. 
 *
 */
#ifndef LIB_H
#define LIB_H

#include "typedef.h"

#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<string.h>
#include<fcntl.h>
#include<unistd.h>
#include<string.h>
#include<limits.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<ctype.h>

/** 
 * @brief Free created linked list after use.
 * @param head Points to head of list.
 */
void free_parsed_input(node_t* head);

/**
 * @brief Reads SEMICOLON (;).
 * @param current Current node.
 * If current operator read is a semicolon,
 * executes current command regardless to others.
 */
void read_semicolon(node_t* current);

/**
 * @brief Reads AND (&&) opeator.
 * @param current Current node.
 * If current operator read is AND operator,
 * executes current command and regarding to success or failure,
 * choose to execute next node.
 */
void read_and(node_t* current);

/**
 * @brief Reads OR (||) operator.
 * @param current Current node.
 * If current operator is OR operator, executes current command and
 * regarding to success or failure, choose to execute next node.
 */
void read_or(node_t* current);

/**
 * @brief Reads pipe (|) operator.
 * @param current Current node.
 * If current operator is pipe operator, executes current command and
 * output is redirected to next command input.
 */
void read_pipe(node_t* current);

/**
 * @brief Reads AMPERSAND (&) operator.
 * @param current Current node.
 * If current operator is ampersand operator, runs current command
 * in background.
 */
void read_ampersand(node_t* current);

/**
 * @brief Removes white spaces.
 * @param str String to clean.
 * @return Trimmed string.
 * As strtok parses input including random characters,
 * retrieved strings are trimmed, thus are cleaned.
 */
char* trim_white_space(char *str);

/**
 * @brief Fork and runs command
 * @param current Current node.
 * @return Created child PID.
 * Forks parent process and created children run commands.
 * Parent process retrieve result sent by children when finished.
 */
pid_t fork_and_execute_command(node_t* current);

/**
 * @brief Built-in command "exit".
 * @return Created child PID.
 * Exits shell.
 */
pid_t exit_shell();

/**
 * @brief Built-in command "cd".
 * @param current Current node.
 * @return Created child PID.
 * Changes current directory.
 */
pid_t change_directory(node_t* current);

/**
 * @brief Built-in command "pwd".
 * @param current Current node.
 * @return Created child PID.
 * Prints working directory.
 */
pid_t print_working_directory(node_t* current);

/**
 * @brief Prints error.
 * @param message Custom error message.
 * @param code Errno code.
 * Custom method that prints pre-formatted error messages.
 */
void print_error(const char* message, unsigned int code);

/**
 * @brief Initializes a node.
 * @return New empty node.
 * Created an empty node and sets its values to starting values.
 */
node_t* init_empty_node();

/**
 * @brief Links nodes.
 * @param previous Last node of list.
 * @param new New created node to add.
 */
void push_node(node_t* previous, node_t* new);

/**
 * @brief Reads an operator.
 * @param token Current token read.
 * @return Operator read.
 * Determines the type of operator encountered.
 * Operator may also be COMMAND-type.
 */
operator_t read_operator(char* token);

/**
 * @brief Runs current command.
 * @param current Current node.
 * @return Created child PID.
 * Runs current command command according to its type.
 * Command may be built-in or specific.
 */
pid_t run_current_command(node_t* current);

/**
 * @brief Determines command type.
 * @param arg Command name.
 * @return Command type.
 */
built_in_t is_built_in(char* arg);

/**
 * @brief Runs the linked list.
 * @param root Points to list's start.
 * As linked list is created, loops over it to run
 * all nodes.
 */
void execute_nodes(node_t* root);

/**
 * @brief Parses user input.
 * @param input Command line entered.
 * @return Start of created linked list.
 * Parses user input using strtok and space as a separator.
 * As input is parsed, linked list is created and returned at the end.
 */
node_t* parse_input(char* input);

/**
 * @brief Displays welcome message.
 */
void display_message();

/**
 * @brief Checks user choosed mode.
 * @param args_count Number of args used to run program.
 * @param args Array of arguments.
 * Checks whether shell is launch in Batch or Interactive mode.
 */
void check_usage_mode(int args_count, char** args);

/**
 * @brief Shell style.
 * If user choose interactive over batch mode, runs the interactive mode.
 */
void use_interactive_mode();
 
#endif
