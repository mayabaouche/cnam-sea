/**
 * \file typedef.h
 * \brief Types & structures definitions.
 * \author Maya.B
 * \version 0.1
 * \date 06 décembre 2019
 *
 * Define structures, types & enumerations used in the project.
 *
 */
#ifndef TYPEDEF_H
#define TYPEDEF_H

#define CONSOLE_BUFFER_SIZE 4096 // MAX CONSOLE BUFFER SIZE
#define MAX_ARGS_COUNT 5 // USED FOR ARGV INITI
#define true 1
#define false 0

typedef int bool;

/**
 * \enum built_in
 * \brief Built-in commands type enumeration.
 */
typedef enum built_in 
{ 	
	CD, /*!< cd */
	PWD, /*!<  pwd */
	ECHO, /*!< echo */
	EXIT, /*!< exit */
	NONE /*!< specific command */
} built_in_t; 

/**
 * \enum operator
 * \brief Control & redirection operators
 */
typedef enum operator 
{ 
	OR, /*!< || */
	AND, /*!< && */
	SEMICOLON, /*!< ; */
	PIPE, /*!< | */
	L_REDIRECT, /*!< < */
	R_REDIRECT, /*!< > */
	DL_REDIRECT, /*!< << */
	DR_REDIRECT, /*!< >> */
	AMPERSAND, /*!< & */
	COMMAND /*!< command */
} operator_t; 

/**
 * \struct node_t
 * \brief Node structure used for command parsing.
 */
typedef struct node
{	
    char** argv;  /*!< argv-like commands parsed */
    unsigned int argc;  /*!< args count */
    unsigned int max_argc;  /*!< maximun args count */
    operator_t control_op; /*!< current control operator read */
    int fd_in; /*!< input command file descriptor */
    int fd_out; /*!< output command file descriptor */
    bool is_executed;  /*!< current command execution status */
    bool success;  /*!< current command successfulness status */
    struct node* next; /*!< next node linked */
} node_t ;

#endif
