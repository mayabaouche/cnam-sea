/**
 * \file shell.c
 * \brief Main.
 * \author Maya.B
 * \version 0.1
 * \date 06 décembre 2019
 */
#include "lib.h"

int main(int argc, char** argv)
{
	display_message(); 
	check_usage_mode(argc, argv);		
	return EXIT_SUCCESS;
}
