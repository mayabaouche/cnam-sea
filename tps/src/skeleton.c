/**
 * \file skeleton.c
 * \brief Basic file operations.
 * \version 0.1
 *
 */
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>
#include<fcntl.h>
#include<unistd.h>
#include<getopt.h>

#include<sys/stat.h>
#include<sys/types.h>
#include<dirent.h> 
#include<grp.h>
#include<pwd.h>
#include<time.h>

typedef int bool; 
#define TRUE 1
#define FALSE 0

#define STDOUT 1
#define STDERR 2

#define MAX_PATH_LENGTH 4096
#define BUF_SIZE 1024

#define USAGE_SYNTAX "[OPTIONS] [-i INPUT] [-o OUTPUT]"
#define USAGE_PARAMS "OPTIONS:\n\
  -i, --input  INPUT_FILE  : input file\n\
  -o, --output OUTPUT_FILE : output file\n\
***\n\
  -v, --verbose : enable *verbose* mode\n\
  -h, --help    : display this help\n\
  -c, --copy	: copy INPUT_FILE into OUTPUT_FILE\n\
  -r, --reverse : reverse INPUT_FILE and displays it\n\
  -l, --list    : list DIR content and details\n"



/**
 * Displays binary usage
 * by printing on stdout all available options
 *
 * \return void
 */
void print_usage(char* bin_name)
{
  dprintf(1, "USAGE: %s %s\n\n%s\n", bin_name, USAGE_SYNTAX, 
	USAGE_PARAMS);
}

/**
 * Copies specified source
 * file into specified destination file  
 * 
 * \param src_file source file
 * \param dst_file destination file
 * \return void
 */
int copy_files(const char* src_file, const char* dst_file)
{
	int src_desc = -1, dst_desc = -1;
	ssize_t src_read;
	char buffer[BUF_SIZE]; 
	
	// obtain file descriptors
	src_desc = open(src_file, O_RDONLY); 	
	dst_desc = open(dst_file, O_CREAT | O_RDWR, S_IRWXU); 
	
	// error
	if (src_desc < 0)
	{
		perror(strerror(errno)); 
		return -1; 
	}
	// error
	if (dst_desc < 0)
	{
		perror(strerror(errno)); 
		return -1; 
	}
	
	// writes content into destination file
	while ((src_read = read(src_desc, buffer, BUF_SIZE)) > 0)
	{
		if (write(dst_desc, buffer, src_read) != src_read)
		{
			perror("Could not write buffer content.\n"); 
		}
	}
	
	// close descriptors
	if (close(src_desc) < 0) 
	{
		perror(strerror(errno));
		return -1; 
	}
	if (close(dst_desc) < 0)
	{
		perror(strerror(errno));
		return -1; 
	}
	return 0;	
}

/**
 * Checks whether a file is a text file or not.
 * 
 * \param file_to_check file name to check
 * \return true if yes, false if no. 
*/
bool is_text_file(const char* file_to_check)
{
	struct stat st; 
	if (stat(file_to_check, &st) == 0 && S_ISREG(st.st_mode))
		return TRUE; 
	return FALSE; 	
}

/**
 * Checks whether a path is a directory or not.
 * 
 * \param path path to check
 * \return true if yes, false if no. 
*/
bool is_directory(const char *path)
{
    struct stat st;
    if (stat(path, &st) == 0 && S_ISDIR(st.st_mode))
		return TRUE;
	return FALSE;     
}
/**
 * Displays reversed specified file content. 
 * 
 * \param file_to_reverse file content to reverse
 * \return void
 */
int print_reversed_file(const char* file_to_reverse)
{
	if (!is_text_file(file_to_reverse))
	{
		perror(strerror(errno)); 
		return -1;  
	}
	
	int file_desc = -1, offset; 
	char c; 
	
	// obtain file descriptor 
	file_desc = open(file_to_reverse, O_RDONLY);
	// error
	if (file_desc < 0)
	{		
		perror(strerror(errno)); 
		return -1;
	}
	
	// set offset to END
	offset = lseek(file_desc, 0, SEEK_END);
	
	// reverse content
	while (offset > 0)
	{
		read(file_desc, &c, 1);
		write(1, &c, 1); 
		lseek(file_desc, -2, SEEK_CUR);
		offset--; 
	}
	printf("\n");

	// close descriptor
	if (close(file_desc) < 0)
	{		
		perror(strerror(errno)); 
		return -1; 
	}	
	return 0;
}


/**
 * Executes ls-behavioured command.
 * 
 * \param path directory's path
 * \return void
 * */
int ls_like(const char* path)
{
	// checks if path is directory
	if (!is_directory(path))
	{
		perror(strerror(errno)); 
		return -1; 
	}
	
	DIR* dir = NULL; 
	struct dirent* files = NULL; 
	struct stat st;
	struct group* grp = NULL; 
	struct passwd* pwd = NULL;
	struct tm* tm = NULL; 
	
	// checks if directory is open
	if ((dir = opendir(path)) == NULL)
	{
		perror(strerror(errno)); 
		return -1; 
	} 
	
	/* while files are opened,
	 * list names, permissions, owner
	 * groups, size, last update 
	 */
	while ((files = readdir(dir)) != NULL) 
	{
		if (strcmp(files->d_name, ".") != 0 &&  strcmp(files->d_name, "..")  != 0)
		{
			stat(files->d_name, &st);
			printf("%s ", files->d_name);
			printf( (st.st_mode & S_IRUSR) ? "r" : "-");
			printf( (st.st_mode & S_IWUSR) ? "w" : "-");
			printf( (st.st_mode & S_IXUSR) ? "x" : "-");
			printf( (st.st_mode & S_IRGRP) ? "r" : "-");
			printf( (st.st_mode & S_IWGRP) ? "w" : "-");
			printf( (st.st_mode & S_IXGRP) ? "x" : "-");
			printf( (st.st_mode & S_IROTH) ? "r" : "-");
			printf( (st.st_mode & S_IWOTH) ? "w" : "-");
			printf( (st.st_mode & S_IXOTH) ? "x" : "-");
			
			/* structures used to retrieve
			 * files informations
			 */
			grp = getgrgid(st.st_gid); 
			pwd = getpwuid(st.st_uid);
			tm = localtime(&(st.st_mtime));
			
			printf(" %s:%s ", pwd->pw_name, grp->gr_name); 
			printf("-%ld- ", st.st_size);
			printf("%02d%02d%02d@%02dh%02d", tm->tm_mday, tm->tm_mon+1, 
				tm->tm_year-100, tm->tm_hour, tm->tm_min);
			printf("\n");
		}
	}
	return 0;
} 
/**
 * Checks if variable must be free
 * (check: ptr != NULL)
 *
 * \param void* to_free pointer to an allocated mem
 * \see man 3 free
 * \return void
 */
void free_if_needed(void* to_free)
{
  if (to_free != NULL) free(to_free);  
}


/**
 *
 * \see man 3 strndup
 * \see man 3 perror
 * \return
 */
char* dup_optarg_str()
{
  char* str = NULL;

  if (optarg != NULL)
  {
    str = strndup(optarg, MAX_PATH_LENGTH);
    
    // Checking if ERRNO is set
    if (str == NULL) 
      perror(strerror(errno));
  }

  return str;
}


/**
 * Binary options declaration
 * (must end with {0,0,0,0})
 *
 * \see man 3 getopt_long or getopt
 * \see struct option definition
 */
static struct option binary_opts[] = 
{
  { "help",    	no_argument,       		0, 'h' },
  { "verbose", 	no_argument,       		0, 'v' },
  { "input",   	required_argument, 		0, 'i' },
  { "output",  	required_argument, 		0, 'o' },
  { "copy",	   	no_argument,		  	0, 'c' },
  {	"reverse", 	no_argument,		  	0, 'r' },   
  {	"list",   	required_argument,		0, 'l' },
  { 0,         	0,                 		0, 	0  } 
};

/**
 * Binary options string
 * (linked to optionn declaration)
 *
 * \see man 3 getopt_long or getopt
 */ 
const char* binary_optstr = "hcvl:ri:o:";



/**
 * Binary main loop
 *
 * \return 1 if it exit successfully 
 */
int main(int argc, char** argv)
{
  /**
   * Binary variables
   * (could be defined in a structure)
   */
  short int is_verbose_mode = 0;
  short int copy_flag = FALSE, reverse_flag = FALSE, list_flag = FALSE;
  char* bin_input_param = NULL;
  char* bin_output_param = NULL;
  char* path_param = NULL; 

  // Parsing options
  int opt = -1;
  int opt_idx = -1;


  while ((opt = getopt_long(argc, argv, binary_optstr, binary_opts, &opt_idx)) != -1)
  {
    switch (opt)
    {
      case 'i':
        //input param
        if (optarg)
        {
          bin_input_param = dup_optarg_str();         
        }
        break;
      case 'o':
        //output param
        if (optarg)
        {
          bin_output_param = dup_optarg_str();
        }
        break;
      case 'v':
        //verbose mode
        is_verbose_mode = TRUE;
        break;
      case 'h':
        print_usage(argv[0]);
        free_if_needed(bin_input_param);
        free_if_needed(bin_output_param);
 
        exit(EXIT_SUCCESS);
      case 'c':
		copy_flag = TRUE;
		break;
      case 'r':
		reverse_flag = TRUE; 
		break;
      case 'l':	  
        //path param
        if (optarg)
        {
          path_param = dup_optarg_str();         
        }
		list_flag = TRUE; 
		break;
      default :
        break;
    }
  } 
  /**
   * Checking binary requirements
   * (could defined in a separate function)
   */
  if ((copy_flag == TRUE || reverse_flag == TRUE) && (bin_input_param == NULL || bin_output_param == NULL))
  {
    dprintf(STDERR, "Bad usage! See HELP [--help|-h]\n");

    // Freeing allocated data
    free_if_needed(bin_input_param);
    free_if_needed(bin_output_param);
    // Exiting with a failure ERROR CODE (== 1)
    exit(EXIT_FAILURE);
  }
  
  // Printing params
  dprintf(1, "** PARAMS **\n%-8s: %s\n%-8s: %s\n%-8s: %d\n%-8s: %d\n%-8s: %d\n%-8s: %d\n", 
          "input",   	bin_input_param, 
          "output",  	bin_output_param, 
          "verbose", 	is_verbose_mode,
          "copy", 		copy_flag, 
          "reverse", 	reverse_flag,
          "list", 		list_flag          
          );

	// Business logic must be implemented at this point
	if (copy_flag == TRUE)
		copy_files(bin_input_param, bin_output_param); 

	if (reverse_flag == TRUE)
		print_reversed_file(bin_input_param);
	
	if (list_flag == TRUE)
	{
		if (path_param == NULL)
		{
			dprintf(STDERR, "Bad usage! See HELP [--help|-h]\n");
			// Freeing allocated data
			free_if_needed(path_param);
			// Exiting with a failure ERROR CODE (== 1)
			exit(EXIT_FAILURE);
		}
		ls_like(path_param);
	}	


  // Freeing allocated data
  free_if_needed(bin_input_param);
  free_if_needed(bin_output_param);


  return EXIT_SUCCESS;
}
