/**
 * \file pingpong.c
 * \brief Programme modélisant un ping pong de signaux. 
 * \version 0.1
 *
 */
#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<string.h>
#include<fcntl.h>
#include<unistd.h>
#include<sys/wait.h> 
#include<time.h>
#include<signal.h>
#include<sys/mman.h>

#define MAX_POINTS 13

pid_t parent_pid = 0;
pid_t child_pid = 0; 

unsigned int* parent_score = 0; 
unsigned int* child_score = 0; 

/**
 * Affiche le message d'erreur et le code d'erreur.
 * \param c message d'erreur
 * \param code errno
 */ 
void print_error(char *c, int code)
{
	printf("Error : %s \nCode : %s", c, strerror(code));
	exit(code);
}

/**
 * Terminaison de la partie.
 */
void game_over()
{	
	// libérations zones mémoires  
  	munmap(parent_score, sizeof *parent_score);
  	munmap(child_score, sizeof *child_score);
	// termine le signal 
	kill(child_pid, SIGTERM);  
	exit(EXIT_SUCCESS); 
}

/**
 * Affiche les scores.
 * \param message commentaire du match
 */
void print_scores(char* message)
{
	printf("\n%s\nScores: P(%d) - C(%d)\n", message, 
					*parent_score, *child_score);
}

/**
 * Handler de signal
 * \param sig signal intercepté
 */
void sig_handler(int sig)
{
	unsigned int ball_catched = rand(); 
	
	// dans le fils, on reçoit SIGUSR2 du père
	if (sig == SIGUSR2 && child_pid == 0)
	{
		// test victoire
		if (*child_score == MAX_POINTS)
		{
			printf("Child won!\n");
			game_over(); 
		}
		
		printf("\nParent attacks!");  
		// aléatoire sur le jeu du fils
		if (ball_catched%2 == 0)
		{			
			print_scores("Child catched the ball!"); 
		}
		else 
		{			
			(*parent_score)++;
			print_scores("Child misses the ball!"); 
		}
		// envoi SIGUSR1 pour riposter	 	
		kill(parent_pid, SIGUSR1);
	}
	
	// dans le père, on reçoit SIGUSR1 du fils
	if (sig == SIGUSR1 && child_pid > 0) 
	{
		// test victoire
		if (*parent_score == MAX_POINTS)
		{
			printf("Parent won!\n");
			game_over(); 
		}		
		printf("\nChild attacks!"); 
		
		// aléatoire sur le jeu du père
		if (ball_catched%2 == 0)
		{
			print_scores("Parent catched the ball!"); 
		}
		else 
		{		
			(*child_score)++; 
			print_scores("Parent misses the ball!");
		}
		// on attend un peu avant de riposter... 
		sleep(3); 	
		// on envoi SIGUSR2 au fils en retour	
		kill(child_pid, SIGUSR2); 
	}
	// fin du jeu forcée par l'utilisateur
	if (sig == SIGINT)
	{
		printf("Programm interrupted.\n"); 
		game_over(); 
	}
}

int main(int argc, char* argv[])
{	
	// création des zones mémoires accessibles par les deux processus 
	child_score = mmap(NULL, sizeof *child_score, PROT_READ | PROT_WRITE, 
             	MAP_SHARED | MAP_ANONYMOUS, -1, 0);
	parent_score = mmap(NULL, sizeof *parent_score, PROT_READ | PROT_WRITE, 
				MAP_SHARED | MAP_ANONYMOUS, -1, 0);
	 
	// check-up 
	if (signal(SIGUSR1, sig_handler) == SIG_ERR)
		print_error("Failed to install SIGUSR1 signal.", errno); 
	if (signal(SIGUSR2, sig_handler) == SIG_ERR)		
		print_error("Failed to install SIGUSR2 signal.", errno); 
	if (signal(SIGINT, sig_handler) == SIG_ERR)		
		print_error("Failed to install SIGINT signal.", errno); 

	// création du fils
	child_pid = fork(); 

	// erreur création fils
	if (child_pid < 0) 
	{
		print_error("Failed to create child process.", errno); 
	}
	// dans le fils
	else if (child_pid == 0)
	{
		// récupère le ppid
		parent_pid = getppid(); 
	}
	// dans le père
	else if (child_pid > 0) 
	{	
		// envoie SIGUSR2 au fils
		kill(child_pid, SIGUSR2); 
	}
	
	while(1);
	
	return 0; 
}
