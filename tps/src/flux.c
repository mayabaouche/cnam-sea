/**
 * \file flux.c
 * \brief Redirection de flux via pipe.
 * \version 0.1
 */
#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<string.h>
#include<unistd.h>
#include<fcntl.h>

#include<sys/types.h>
#include<sys/wait.h>

#define MESSAGE 19

int main(int argc, char* argv[]){
	
	// descripteur du pipe 
	int pipe_fd[2]; 
	
	// création du pipe
	pipe(pipe_fd);
	
	// fils dans lesquels vont s'exécuter les commandes
	pid_t ps_fils = fork(); 
	pid_t grep_fils = fork();
	
	int ps_status, grep_status;
	
	// on est dans ps_fils
	if (ps_fils == 0)
	{		
		// fermeture du pipe en lecture
		close(pipe_fd[0]); 
		// redirection vers la sortie standard 
		dup2(pipe_fd[1], 1);
		// fermeture du pipe en écriture
		close(pipe_fd[1]);
		
		// execution la commande ps avec ses arguments 
		execlp("ps", "ps", "eaux", (char*)NULL);
		
		// en cas de retour de l'exec, affichage de l'erreur
		perror(strerror(errno)); 
		exit(EXIT_FAILURE);
	}
	
	// on est dans grep_fils 	
	if (grep_fils == 0)
	{		
		// on ferme le tube en écriture
		close(pipe_fd[1]); 		
		
		// redirection vers l'entrée standard 
		dup2(pipe_fd[0], 0);
		
		// récupère le descripteur fichier  
		int file = open("/dev/null", O_WRONLY);
		// errueur
		if (file < 0) 
		{
			perror(strerror(errno));
			exit(EXIT_FAILURE);
		}
		// redirection vers la sortie standard
		dup2(file, 1); 
		
		// On execute la commande grep avec ses arguments
		execlp ("grep", "grep", "^root", (void*)0);
		
		// en cas de retour de l'exec, affichage de l'erreur
		perror(strerror(errno)); 
		exit(EXIT_FAILURE);
	}	
					
	// on ferme le pipe 
	close(pipe_fd[0]);
	close(pipe_fd[1]);
	 
	// on attend la fin des fils 	
	waitpid(ps_fils, &ps_status, 0);
	waitpid(grep_fils, &grep_status, 0);
	
	// si tout s'est bien passé 
	if (WIFEXITED(ps_status) && WEXITSTATUS(ps_status) == 0 
			&& WEXITSTATUS(grep_status) && WIFEXITED(grep_status) == 0) 
		write(1, "root est connecté\n", MESSAGE);
	
	return 0;
}
