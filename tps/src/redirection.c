/**
 * \file redirection.c
 * \brief Redirection standard.
 * \version 0.1
 *
 */
#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<string.h>
#include<fcntl.h>
#include<unistd.h>
#include<sys/wait.h> 


int main(int argc, char* argv[])
{
	// test sur le nombre d'arguments
	if (argc < 2)
	{
		perror(strerror(errno));
		exit(EXIT_FAILURE);
	}
	
	// afficher l'argument saisi
	printf("Argument passé: %s\n", argv[1]);
	
	// création du fils
	pid_t p_fils = fork();
	
	// nom du fichier temporaire
	char TEMPLATE[] = "../tmp/proc-exerciseXXXXXX";
	 
	// file descriptor du fichier temporaire
	int fd = -1;
	
	// utilisé pour attendre le retour du fils
	int status;
	
	if (p_fils < 0)
	{
		perror(strerror(errno));
		exit(EXIT_FAILURE); 
	}
	// si on est dans le fils
	if (p_fils == 0)
	{
		// création du fils
		printf("Je suis le fils créé et mon PID est %d.\n", getpid());
		
		// ferme le descripteur STDERR
		close(2);
		
		// création du fichier temporaire
		fd = mkstemp(TEMPLATE);
		
		// link de fd et STDOUT, écriture dans fd
		printf("Descripteur ouvert : %d\n", dup2(fd, 1));
		
		/* programme lancé avec un ls en argv[1], résultat
		 * de la commande exec dans fd */
		execvp(argv[1], argv+1);		
		// retour si execvp n'a pas réussi
		perror(strerror(errno));
		exit(EXIT_FAILURE);
	}
	// si on est dans le pere
	else if (p_fils	> 0)
	{
		// affichage du PID du père
		printf("Je suis le père et mon PID est %d.\n", getpid());
		
		// attend la fin du fils
		wait(&status);
		
		// si fils est terminé
		if (WIFEXITED(status) && WEXITSTATUS(status) == 0)
			// affichage du message
			printf("That's All Folks!\n");
	}
	
	return 0;
}
