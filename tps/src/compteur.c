/**
 * \file compteur.c
 * \brief Programme comptant le nombre de signal de type SIGINT reçu. 
 * \version 0.1
 *
 */
#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<string.h>
#include<fcntl.h>
#include<unistd.h>
#include<sys/wait.h> 
#include<time.h>

unsigned int count; 

/**
 * Affiche le message et le code d'erreur.
 * \param c message d'erreur
 * \param code errno 
 */
void print_error(char *c, int code)
{
	printf("Error : %s \nCode : %s", c, strerror(code));
	exit(code);
}
/**
 * Handler de signal.
 * \param sig signal reçu
 */
void sig_int_received(int sig)
{
	if (sig == SIGINT)
	{
		count++; 
		printf("Valeur du compteur : %d\n", count); 
	}	
	else if (sig == SIGTERM) 
	{
		printf("SIGTERM reçu. Fin du programme!\n"); 
		exit(EXIT_SUCCESS); 
	}	
}

int main(int argc, char* argv[])
{	
	// init
	count = 0; 
	printf("Send ^C to increment the counter value.\n"); 
	printf("Use kill -15 %d to end program...\n", getpid());
	
	// set signal handler 
	sigset_t sig_proc;
	struct sigaction action; 
	
	sigemptyset(&sig_proc); 
	action.sa_mask = sig_proc; 
	action.sa_flags = 0; 
	action.sa_handler = sig_int_received; 
	
	sigaction(SIGINT, &action, NULL); 
	sigaction(SIGTERM, &action, NULL); 
	
	// runs while the process receives the set-up signals
	while (1);
	
	return 0;
}
