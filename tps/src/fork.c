/**
 * \file fork.c
 * \brief Fork yourself.
 * \version 0.1
 *
 */
#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<string.h>
#include<unistd.h>
#include<sys/wait.h> 

int main(int argc, char* argv[]){
	
	// status
	int status; 	
	// creation du fils  
	pid_t p_fils = fork();	

	// echec du fork 
	if (p_fils < 0)
	{
		perror(strerror(errno));
		exit(EXIT_FAILURE);
	}
	// on est dans le fils
	else if (p_fils == 0)
	{
		printf("Je suis le fils. Mon PID est %d et celui de mon père est %d.\n", getpid(), getppid());
		exit(getpid()%10);
	}
	// on est dans le père
	else {
		printf("Je suis le père (mon PID = %d) et le PID de mon fils est %d.\n", getpid(), p_fils);	
		wait(&status);
		
		if (WIFEXITED(status)) 
			printf("Code retour du fils: %d\n", WEXITSTATUS(status)); 
	}
	return 0;
}
