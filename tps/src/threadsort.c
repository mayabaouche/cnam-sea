/**
 * \file threadsort.c
 * \brief Threaded sort. 
 * \version 0.1
 *
 */
#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<string.h>
#include<fcntl.h>
#include<unistd.h>
#include<sys/wait.h> 
#include<time.h>
#include<pthread.h>
#include<limits.h>

#define SIZE 10 // taille du tableau
#define THREAD_COUNT 4 // nombre de threads 
#define MIN_VALUE 0 // valeur minimale existante dans le tableau
#define MAX_VALUE 20 // valeur maximale existante dans le tableau

// resultat du parcours
typedef struct result {
	int min; 
	int max; 
} result_t;  

// arguments des threads
typedef struct thread_data {
	unsigned int* tab;
	unsigned int index; 
	pthread_mutex_t min; 
	pthread_mutex_t max; 
}thread_data_t; 

// initialise le resultat de la recherche
result_t resulat = { .min = INT_MIN, .max = INT_MAX }; 

// tableau d'entiers
unsigned int tab[SIZE];
unsigned int min_value; 
unsigned int max_value; 

/**
 * Affiche un message d'erreur et le code d'erreur.
 * \param c message d'erreur
 * \param code errno
 */
void print_error(char *c, int code)
{
	printf("Error : %s \nCode : %s", c, strerror(code));
	exit(code);
}
/**
 * Initialise le tableau avec des entiers aléatoires.
 * \param tab tableau d'entiers
 */     
void init_tab(unsigned int* tab)
{
	srand(time(NULL));
	printf("Generated tab:\n"); 
	for (unsigned int i = 0; i < SIZE; i++)
	{
		tab[i] = (rand() % (MAX_VALUE-MIN_VALUE+1)) + MIN_VALUE; 		
		printf("%d ", tab[i]);
	}
	min_value = tab[0]; 
	max_value = tab[0];	
}

/**
 * Recherche des valeurs minimale et maximale du tableau.
 * \param th_data argument du thread
 */
void* search_minmax(void* th_data)
{
	thread_data_t* args = th_data; 
	for(unsigned int i = 0; i < args->index; i++)
	{
		// section critique protégée par le mutex
		pthread_mutex_lock(&args->min);
		if (args->tab[i] < min_value)
		{
			min_value = args->tab[i];
			resulat.min = args->tab[i];
		}
		pthread_mutex_unlock(&args->min);
		// fin 
		
		// section critique protégée par le mutex
		pthread_mutex_lock(&args->max);
		if (args->tab[i] > max_value)
		{
			max_value = args->tab[i]; 
			resulat.max = args->tab[i];
		}
		pthread_mutex_unlock(&args->max);
		// fin
	}
	pthread_exit(NULL);
}

void create_sort_threads(thread_data_t* args, pthread_t* threads)
{	
	printf("\n-- Threads initialization --\n");
	for (unsigned int i = 0; i < THREAD_COUNT; i++)
	{
		if (pthread_mutex_init(&args[i].min,NULL) != 0)
			print_error("Failed to init min mutex.", errno); 
	
		if (pthread_mutex_init(&args[i].max,NULL) != 0)
			print_error("Failed to init max mutex.",errno);
		
		// initialisation des threads
		args[i].tab = tab+((SIZE/THREAD_COUNT)*i);
		args[i].index = (SIZE/THREAD_COUNT);
		if(i+1 == THREAD_COUNT) 
			args[i].index += SIZE%THREAD_COUNT;	
		pthread_create(threads+i, NULL, search_minmax, &args[i]);
		printf("Thread %d created successfuly!\n", i); 
	}
	// on attend la fin de tous les threads
	for (unsigned int i = 0; i < THREAD_COUNT; i++)
	{
		pthread_join(threads[i], NULL);
	}
}

int main(int argc, char* argv[])
{	
	init_tab(tab); 	
	pthread_t threads[THREAD_COUNT]; 	
	thread_data_t args[THREAD_COUNT];
	
	create_sort_threads(args, threads); 
	
	printf("Results:\n"); 
	printf("min : %d\nmax : %d\n", resulat.min, resulat.max);
	return 0;
}
