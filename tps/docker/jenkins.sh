# récupère l'image Jenkins 
docker pull jenkins
# lance le serveur Jenkins 
docker run --name tp_jenkins -p 8080:8080 -d jenkins
# teste le bon fonctionnement de Jenkins
curl http://localhost:8080 > /dev/null 2>&1 && echo "OK" || echo "KO" 1>&2
# stop Jenkins
docker stop tp_jenkins
