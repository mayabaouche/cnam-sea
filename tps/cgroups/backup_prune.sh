#!/bin/bash

# create config file
touch /etc/cgsnapshot_blacklist.conf

# create a snapshot
cgsnapshot -s blkio memory > snapshot.conf

# check existance
lscgroup | grep tp1

# deletion
sudo cgdelete blkio:/tp1_blkio
sudo cgdelete memory:/tp1_mem

# check existance
lscgroup | grep tp1
