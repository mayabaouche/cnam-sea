#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

int main(int argc, char** argv) {
	
	if(argc != 2) {
		printf("USAGE: %s <bytes count>\n",argv[0]);
		return 1;
	}
	
	char *endptr;
	long m = strtol(argv[1],&endptr,10);
	
	if(argv[1] == endptr || m < 0) {
		printf("ERROR: Please enter a valid value\n");
		return 1;
	}
	
	dprintf(2,"%d\n",getpid());
	
	void* p = malloc(sizeof(char)*m);
	
	sleep(60);
	
	free(p);
	return 0;
}
