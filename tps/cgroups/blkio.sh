#!/bin/bash

# apt install iotop

# apt install cgroup-tools

# benchmark
dd if=/dev/urandom of=test_blkio oflag=direct  bs=4k count=500000

# create cgroup in blkio controller
mkdir -p /sys/fs/cgroup/blkio/tp1_blkio

# check major and minor ids of /dev/sda (should be 8 and 0)
ls -l /dev/sda

# limiting io read speed
echo "8:0 5000000" > /sys/fs/cgroup/blkio/tp1_blkio/blkio.throttle.read_bps_device

# limiting io write speed
echo "8:0 5000000" > /sys/fs/cgroup/blkio/tp1_blkio/blkio.throttle.write_bps_device

# benchmark with specific cgroup context
cgexec -g blkio:tp1_blkio dd if=/dev/urandom of=test_blkio oflag=direct  bs=4k count=500000
