#!/bin/bash

# apt install iotop

# apt install cgroup-tools

# compile
# gcc mem.c -o mem

# setup cgroup fot memory contention
mkdir -p /sys/fs/cgroup/memory/tp1_mem 
echo "5M" > /sys/fs/cgroup/memory/tp1_mem/memory.limit_in_bytes

# exec gedit in related cgroup
cgexec -g memory:tp1_mem gedit &

# show PID
ps -aux | grep gedit
sleep 3

# htop exec to check results
htop


